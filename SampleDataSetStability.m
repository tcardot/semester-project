% SampleDataSetStability : compute the score of the reference set and fit a
% quadratic discriminant(unfinished)
% 
% 
% 



arraysBeforeLesion = load('arraysBeforelesion.mat');
arraysAfterLesion = load('arraysAfterLesion.mat');

%concatenate all channels
electrodes = [arraysBeforeLesion.array1;arraysBeforeLesion.array2; ...
    arraysBeforeLesion.array3; arraysBeforeLesion.array4; ...
    arraysBeforeLesion.array5; arraysBeforeLesion.array6];

%keep only units with firing rate >= 1Hz
rates = computeElectrodeFiringRate(electrodes);
electrodes = electrodes(rates >= 2);

%true-negatives set
allMus = zeros(size(electrodes,1),1);
allSigmas = zeros(size(electrodes,1),1);
allWaveforms = zeros(size(electrodes,1),48);
for i = 1:size(allMus,1)
     [t_mu,t_sig,t_wf] = UnitStabilityFeatures(electrodes(i));
     allMus(i) = t_mu;
     allSigmas(i) = t_sig;
     allWaveforms(i,:) = t_wf;
end
%averages to normalize isih score
stdMu = std(allMus);
stdSigma = std(allSigmas);
compIndices = nchoosek(1:length(electrodes),2);
trueNegatives = zeros(size(compIndices,1),2);

for i = 1: size(compIndices,1)
    %compute ISIH score
    trueNegatives(i,1) = sqrt(((allMus(compIndices(i,1)) -allMus(compIndices(i,2))).^2)/stdMu^2 + ...
        ((allSigmas(compIndices(i,1)) -allSigmas(compIndices(i,2))).^2)/stdSigma^2);
    %compute waveform score
    negCorrmat = corrcoef(allWaveforms(compIndices(i,1),:),allWaveforms(compIndices(i,2),:));
    %disp(corrmat);
    trueNegatives(i,2) = negCorrmat(2,1);
    
end
trueNegatives(:,1) = log(trueNegatives(:,1));
trueNegatives(:,2) = atanh(trueNegatives(:,2));



%true-positives set
pSet(1).set = electrodes;
pSet(2).set = electrodes;
pSet(3).set = electrodes;
pSet(4).set = electrodes;
pSet(5).set = electrodes;
for i = 1:size(electrodes,1)
    pSet(1).set(i).spikes = pSet(1).set(i).spikes(1:10);
    pSet(2).set(i).spikes = pSet(2).set(i).spikes(11:20);
    pSet(3).set(i).spikes = pSet(3).set(i).spikes(21:30);
    pSet(4).set(i).spikes = pSet(4).set(i).spikes(31:40);
    pSet(5).set(i).spikes = pSet(5).set(i).spikes(41:50);
end

permIndices = nchoosek(1:5,2);
truePositives = zeros(size(permIndices,1)*size(electrodes,1),2);
positivesIndex = 1;
for i = 1:size(electrodes,1)
    for j = 1:size(permIndices,1)
        
        %check sets are big enough
        nSpikes1 =  sum(arrayfun(@(y) length(y.timeStamps),pSet(permIndices(j,1)).set(i).spikes));
        nSpikes2 =  sum(arrayfun(@(y) length(y.timeStamps),pSet(permIndices(j,2)).set(i).spikes));
        if nSpikes1 >= 25 && nSpikes2 >= 25 
            
            % ISIH score
            [mu1,sig1,wf1] = UnitStabilityFeatures(pSet(permIndices(j,1)).set(i));
            [mu2,sig2,wf2] = UnitStabilityFeatures(pSet(permIndices(j,2)).set(i));
            truePositives(positivesIndex,1) = sqrt(((mu1 - mu2).^2)/stdMu + ...
                ((sig1 -sig2).^2)/stdSigma);

            %waveform score
            posCorrmat = corrcoef(wf1,wf2);
            truePositives(positivesIndex,2) = posCorrmat(2,1);
            if posCorrmat(2,1) < 0.5
                disp(posCorrmat(2,1));
            end
            positivesIndex = positivesIndex + 1;
        end
        
            
    end
end


%truePositives(truePositives(:,1) == 0) = [];
%transform
truePositives(:,1) = log(truePositives(:,1));
truePositives(:,2) = atanh(truePositives(:,2));
%remove zeros
truePositives = truePositives(truePositives(:,1) > -100,:);
muNeg = mean(trueNegatives,1);
muPos = mean(truePositives,1);
covNeg = cov(trueNegatives);
covPos = cov(truePositives);

discrimFun =  @(x,y) ([y;x]-muPos.').' * inv(covPos)*([y;x]-muPos.') - ([y;x]-muNeg.').'* inv(covNeg)*([y;x]-muNeg.');
%compute stability score for true positives
tPosScores = zeros(size(truePositives,1),1);
for i = 1:length(tPosScores)
    tPosScores(i) = discrimFun(truePositives(i,2),truePositives(i,1));
end
threshScore = mean(tPosScores) + 2*std(tPosScores);
discrimFun2 =  @(x,y) ([y;x]-muPos.').' * inv(covPos)*([y;x]-muPos.') - ([y;x]-muNeg.').'* inv(covNeg)*([y;x]-muNeg.')- threshScore;
disp('threshold:');
disp(threshScore);

scatter(trueNegatives(:,2),trueNegatives(:,1));
hold on;
%disp(length(truePositives(truePositives(:,1) < -10,1)));
scatter(truePositives(:,2),truePositives(:,1),'+');
for i = [2,3,4,5]
    hold on;
    [wscore,iscore] = computeStabilityScore(i);
    scatter(wscore,iscore,'*');
end
%{
figure();
for i = 1:size(allWaveforms,1)
    hold on
    plot(allWaveforms(i,:));
end
%}
ezplot(discrimFun2);



