function features = extractVelocityAndForce(day,recordIndices,velAxis,forceAxis)

% extractVelocityAndForce : extract instant velocity and force during the
% tasks
%
% INPUT ARGUMENTS:
% 
% day : day of recording ('yyyy.mm.dd')
% recordIndices: indices of the recordings to be extracted
% 
% RETURN:
% 
% features : structure with 2 fields: 'forces' and 'velocities'
% 
%   velocities : array struct with 1 field: 'instantVelocity' , each line
%   is the instant velocity during one task
%   
%   forces : array struct with 1 field: 'instantForce' , each line is the
%   instant force force during one task
% 
%   velAxis : axis of the velocity, can be 'x', 'y', 'z' or 'abs
% 
% forceAxis : axis of the force, can be 'x', 'y', 'z' or 'abs
%



viconDir = fullfile('C:\Users\Toussain\Documents\EPFL_projet_semestre',day,'VICON');
eventDir = fullfile('C:\Users\Toussain\Documents\EPFL_projet_semestre',day,'Event');

viconMats = [];
eventMats = [];

binSize = 0.01;

for i = 1:length(recordIndices)
    if isempty(viconMats)
        viconMats = load(fullfile(viconDir,strcat(day,'_Jyn_Brain_Injury', sprintf('%02d',recordIndices(i)),'.mat'))); 
    else
        viconMats = [viconMats;load(fullfile(viconDir,strcat(day,'_Jyn_Brain_Injury', sprintf('%02d',recordIndices(i)))))];
    end 
    
    if isempty(eventMats)
        eventMats = load(fullfile(eventDir,strcat(day,'_Jyn_Brain_Injury',sprintf('%02d',recordIndices(i)),'_Struct.mat')));
    else
        eventMats = [eventMats;load(fullfile(eventDir,strcat(day,'_Jyn_Brain_Injury',sprintf('%02d',recordIndices(i)),'_Struct.mat')))];
    end
end

velocities = {};
forces = {};

for i = 1:length(viconMats)
    
    kinematicRes = viconMats(i).kinematic.framerate;
    analogRes = viconMats(i).analog.framerate;

    for j = 1:size(eventMats(i).structTrials,2)

        velBinSize = kinematicRes * binSize;
        forceBinSize = analogRes * binSize;
        %compute begin index and number of bins
        velBegin = eventMats(i).structTrials(j).beginningOfMovement;
        nVelBins = ceil(double(eventMats(i).structTrials(j).LightOFF - eventMats(i).structTrials(j).beginningOfMovement)/double(velBinSize));
        forceBegin = eventMats(i).structTrials(j).beginningOfMovement *(double(analogRes)/double(kinematicRes));
        nForceBins = ceil(double(eventMats(i).structTrials(j).LightOFF - eventMats(i).structTrials(j).beginningOfMovement)*(analogRes/kinematicRes)/double(forceBinSize));
        
        %velocity is from WRB
        markerIndex = 4;
        
        xVel = diff(viconMats(i).kinematic.x(velBegin:velBegin+ nVelBins*velBinSize,markerIndex));
        yVel = diff(viconMats(i).kinematic.y(velBegin:velBegin+ nVelBins*velBinSize,markerIndex));
        zVel = diff(viconMats(i).kinematic.z(velBegin:velBegin+ nVelBins*velBinSize,markerIndex));
        absVel = sqrt(xVel.^2 + yVel.^2 + zVel.^2);
        
        xForceIndex = find(ismember(viconMats(i).analog.labels,'Voltage.Kuka_Fx'));
        yForceIndex = find(ismember(viconMats(i).analog.labels,'Voltage.Kuka_Fy'));
        zForceIndex = find(ismember(viconMats(i).analog.labels,'Voltage.Kuka_Fz'));
        
        xForce = viconMats(i).analog.data(forceBegin:forceBegin+ nForceBins*forceBinSize,xForceIndex);
        yForce = viconMats(i).analog.data(forceBegin:forceBegin+ nForceBins*forceBinSize,yForceIndex);
        zForce = viconMats(i).analog.data(forceBegin:forceBegin+ nForceBins*forceBinSize,zForceIndex);
        absForce = sqrt(xForce.^2 + yForce.^2 + zForce.^2); 
        
        %bin the velocities
        binnedVel  = zeros(nVelBins,1);
        for v = 0:nVelBins-1
            switch velAxis
                case 'x'
                    binnedVel(v+1) = mean(xVel((1 + v*velBinSize):((v+1)*velBinSize)));
                case 'y'
                    binnedVel(v+1) = mean(yVel((1 + v*velBinSize):((v+1)*velBinSize))); 
                case 'z'
                    binnedVel(v+1) = mean(zVel((1 + v*velBinSize):((v+1)*velBinSize)));
                case 'abs'
                    binnedVel(v+1) = mean(absVel((1 + v*velBinSize):((v+1)*velBinSize)));
                otherwise
                    disp('wrong velocity axis argument');
            end
        end
        %bin the forces
        binnedForce = zeros(nForceBins,1);
        for f = 0:nForceBins-1    
            switch forceAxis
                case 'x'
                    binnedForce(f+1) = mean(xForce((1 + f*forceBinSize):((f+1)*forceBinSize)));
                case 'y'
                    binnedForce(f+1) = mean(yForce((1 + f*forceBinSize):((f+1)*forceBinSize)));
                case 'z'
                    binnedForce(f+1) = mean(zForce((1 + f*forceBinSize):((f+1)*forceBinSize)));
                case 'abs'
                    binnedForce(f+1) = mean(absForce((1 + f*forceBinSize):((f+1)*forceBinSize)));
                otherwise
                    disp('wrong force axis argument'),
            end
        end
        
        %add to the struct
        if isempty(velocities)
            velocities.instantVelocity = binnedVel;
        else
            tVelStruct.instantVelocity = binnedVel;
            velocities = [velocities;tVelStruct];
        end
        
        if isempty(forces)
            forces.instantForce = binnedForce;
        else
            tForceStruct.instantForce = binnedForce;
            forces = [forces;tForceStruct];
        end
    end
end

features.forces = forces;
features.velocities = velocities;

end

