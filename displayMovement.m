recordIndices = 13:34;
recordIndices(7) = [];
%feats = extractVelocityAndForce('20190516',recordIndices);
feats = load('feats20190604');
feats = feats.feats20190604;
minVelLength = min(arrayfun(@(x) length(x.instantVelocity), feats.velocities));
minForceLength = min(arrayfun(@(x) length(x.instantForce), feats.forces));


allVels = zeros(size(feats.velocities,1),minVelLength);
allForces = zeros(size(feats.forces,1),minForceLength);

figure();
for i = 1:size(feats.velocities,1)
    
    xVel = ricampiona(feats.velocities(i).instantVelocity,minVelLength);
    hold on
    plot(xVel);
    allVels(i,:) = xVel;
 
    
end
hold on
plot(mean(allVels),'red')
title('velocities');
figure();
for i = 1:20
    xForce = ricampiona(feats.forces(i).instantForce,minForceLength);
    hold on
    plot(xForce);
    allForces(i,:) = xForce;
end
hold on
plot(mean(allForces),'red');
title('forces');