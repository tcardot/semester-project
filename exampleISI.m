arraysBeforeLesion = load('ArraysBeforelesion');


%array 2

[maxElec,maxElecIndex] = max(computeElectrodeFiringRate(ArraysBeforeLesion.array2));
disp(maxElecIndex);
spikeRes = 30000;
%concatenate isi for all trials
isi = []; 
for i = 1: size(ArraysBeforeLesion.array2(maxElecIndex).spikes,1)
    t_isi = diff(double(ArraysBeforeLesion.array2(maxElecIndex).spikes(i).timeStamps) /double(spikeRes));
    if ~isempty(t_isi)   
        if isempty(isi)
            isi = t_isi;
        else
            isi = [isi,t_isi];
        end
    end
end

n_isi = [];
for i = 1: size(ArraysAfterLesion.array2(maxElecIndex).spikes,1)
    t_isi = diff(double(ArraysAfterLesion.array2(maxElecIndex).spikes(i).timeStamps) /double(spikeRes));
    if ~isempty(t_isi)   
        if isempty(n_isi)
            n_isi = t_isi;
        else
            n_isi = [n_isi,t_isi];
        end
    end
end
figure();
hist(log(isi),100);
title('before lesion');
figure();
hist(log(n_isi),100);
title('after lesion');