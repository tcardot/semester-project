function [RasterPlot,TimeStampElec] = reshape_spikes(NEV,nCh)

time = [1:NEV.MetaTags.DataDuration];
RasterPlot = zeros(nCh,length(time));

for ch = 1:nCh
    prov = NEV.Data.Spikes.TimeStamp(find(NEV.Data.Spikes.Electrode==ch));
    RasterPlot(ch,prov) = 1;
    TimeStampElec{ch} = prov;
end


