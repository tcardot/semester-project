function PlayWithNEV()

addpath(genpath('/Volumes/Boekje/Code/NPMK 2.6.0/'))

% open NEV
NEV = openNEVCervical(fullfile(data_dir,[file_name '.nev']),'nosave');

% save NEV
save([file_path MetaTags.NEVlist{iFile}(1:end-4)],'NEV')