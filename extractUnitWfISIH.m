
function unitStruct = extractUnitWfISIH(unit,channel,electrode, NSP1_mats,NSP2_mats, vicon_mats)
% 
% extractUnitWfISIH : extract the waveforms and the ISIHs of the desired
% recording
% 
% RETURN
% 
% an array struct with 2 fields 'ISIH' and 'waveforms. ISIH contains all
% inervals between spikes and waveforms contains all waveforms from the
% recordings, each collumn is a waveform
% 
% 


   [NSP_index, lChan] = findLocalChannel(channel,electrode,NSP1_mats(1),NSP2_mats(1));
   if NSP_index == 1
       NSP_mats = NSP1_mats;
   else
       NSP_mats = NSP2_mats; 
   end
   
   unitStruct = extractRecordWfISIH(NSP_mats(1),vicon_mats(1),NSP_index,unit,lChan);
   
   for t = 2:length(NSP_mats)
       %unitStruct = [unitStruct;extractSingleRecord(NSP_mats(t),vicon_mats(t),NSP_index,unit,lChan)];
       rStruct = extractRecordWfISIH(NSP_mats(t),vicon_mats(t),NSP_index,unit,lChan);
       unitStruct.waveforms = [unitStruct.waveforms, rStruct.waveforms];
       unitStruct.ISIH = [unitStruct.ISIH, rStruct.ISIH];

   end

    function record_struct = extractRecordWfISIH(NSP_mat,VICON_mat,NSP_number,unit,lchannel)
        
        spike_res = NSP_mat.NEV.MetaTags.SampleRes;
        sync_res = VICON_mat.analog.framerate;

        
        %keep only spikes from desired unit
        timestamps = NSP_mat.NEV.Data.Spikes.TimeStamp(NSP_mat.NEV.Data.Spikes.Electrode == lchannel & NSP_mat.NEV.Data.Spikes.Unit == unit);
        waveforms = NSP_mat.NEV.Data.Spikes.Waveform(:,NSP_mat.NEV.Data.Spikes.Electrode == lchannel & NSP_mat.NEV.Data.Spikes.Unit == unit);
        %sync
        NSP_voltage_index = 9*(NSP_number -1) + 2 - NSP_number;

        [~,max_nsp_index] = max(diff(VICON_mat.analog.data(:,NSP_voltage_index)));
        timestamps = timestamps - floor(max_nsp_index * spike_res/sync_res);
        %convert to seconds
        timestamps = double(timestamps)/double(spike_res);
        record_struct.waveforms = waveforms;
        record_struct.ISIH = diff(timestamps);
    end

  
end



