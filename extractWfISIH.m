function dataStruct = extractWfISIH(day,record_indices)
% 
% extractWfISIH : extract the waveforms and the ISIH of the entire
% recordings
% 
% 
%INPUT ARGUMENTS:
%
%day: day of recording ('yyyy.mm.dd')
%record_indices : indices of the recordings to be extracted

%load  NSP, VICON and Event matrices
day_dir = fullfile('D:','JYN',day);
NSP1_dir = fullfile(day_dir,'BLACKROCK_NSP1');
NSP2_dir = fullfile(day_dir,'BLACKROCK_NSP2');
event_dir = fullfile(day_dir,'Event');
vicon_dir = fullfile(day_dir,'VICON');

NSP1_mats = [];
NSP2_mats = [];
event_mats = [];
vicon_mats = [];
for i = 1:length(record_indices)
    if isempty(NSP1_mats)
        NSP1_mats = load(fullfile(NSP1_dir,strcat('ReTh_',day,'JynBrainNSP10',sprintf('%02d',record_indices(i)),'.mat')));
    else
        %mat_name = strcat('ReTh_',day,'JynBrainNSP10',sprintf('%02d',record_indices(i)),'.mat');
        NSP1_mats = [NSP1_mats;load(fullfile(NSP1_dir,strcat('ReTh_',day,'JynBrainNSP10',sprintf('%02d',record_indices(i)),'.mat')))];
    end
    
    if isempty(NSP2_mats)
        NSP2_mats = load(fullfile(NSP2_dir,strcat('ReTh_',day,'JynBrainNSP20',sprintf('%02d',record_indices(i)),'.mat')));
    else
        NSP2_mats = [NSP2_mats;load(fullfile(NSP2_dir,strcat('ReTh_',day,'JynBrainNSP20',sprintf('%02d',record_indices(i)))))];
    end
    
    if isempty(vicon_mats)
        vicon_mats = load(fullfile(vicon_dir,strcat(day,'_Jyn_Brain', sprintf('%02d',record_indices(i)),'.mat'))); 
    else
        vicon_mats = [vicon_mats;load(fullfile(vicon_dir,strcat(day,'_Jyn_Brain', sprintf('%02d',record_indices(i)))))];
    end 
    if isempty(event_mats)
        event_mats = load(fullfile(event_dir,strcat(day,'_Jyn_Brain',sprintf('%02d',record_indices(i)),'_Struct.mat')));
    else
        event_mats = [event_mats;load(fullfile(event_dir,strcat(day,'_Jyn_Brain',sprintf('%02d',record_indices(i)),'_Struct.mat')))];
    end
end 

%{
channels = [];
if elec_number == 1 
    channels = 1:48; 
elseif elec_number == 2
    channels = 49:96;
elseif elec_number == 3
    channels = 97:144;
elseif elec_number == 4
    channels = 145:176;
elseif elec_number == 5
    channels = 177:224;
else
    channels = 225:256;
end
%} 
channels = 1:256;
units = zeros(length(channels),1);
for i = 1: length(channels)

    elec_number = findElecNumber(i);

    [NSP_index, lChan] = findLocalChannel(channels(i),elec_number,NSP1_mats(1),NSP2_mats(1));
    if NSP_index == 1
        units(i) = NSP1_mats(1).NEV.ElectrodesInfo(lChan).Units;
    else
        units(i) = NSP2_mats(1).NEV.ElectrodesInfo(lChan).Units;
    end


end

dataStruct = {};
%append units to the structure
for i = 1:length(channels)
    for j = 1:units(i)
        elec_number = findElecNumber(i);
        t_struct.channel= channels(i);
        t_struct.unit = j;
        t_struct.data = extractUnitWfISIH(j,channels(i),elec_number,NSP1_mats,NSP2_mats,vicon_mats);
        
        if isempty(dataStruct)
            dataStruct = t_struct;
        else
            dataStruct = [dataStruct; t_struct];
        end
    end
end

    function n = findElecNumber(channel)
    if channel <= 48
        n = 1;
    elseif channel <= 96
        n = 2;
    elseif channel<= 144
        n = 3;
    elseif channel <= 176
        n = 4;
    elseif channel <= 224
        n = 5;
    else
        n = 6;
    end
 end
end



