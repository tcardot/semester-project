% tuningCurves : plot the tuning curves for the 5 most active neurons in
% each electrode array
% 
% 2 graphs are plotted for each neuron: velocity along x-axis vs firing rate 
% and force along x-axis vs firing rate



arrays = load('arrays20190604.mat','arrays20190604');
arrays = arrays.arrays20190604;
%arrays = load('arrays20190516.mat','arrays20190516');
%arrays = arrays.arrays20190516;

feats = load('feats20190604');
feats = feats.feats20190604;
%feats = load('feats20190516');
%feats = feats.feats20190516;

flags = load('moveFlags20190604');
flags = flags.moveFlags20190604;
%flags= load('moveFlags20190516');
%flags = flags.moveFlags20190516;

sizes = [ 0 0 0];

%for i = 1:6
for i = 3
    %select 5 most active units
    meanRates = computeElectrodeFiringRate(arrays(i).array);
    [~,sortedIndices] = sort(meanRates);
    sUnits = arrays(i).array(sortedIndices((end-4):end));

    figure();
    for j = 1:5
  
        instantRates = UnitInstantFiringRate(sUnits(j));
        
        %plot velocity
        subplot(2,5,j);
        for t = 1:size(instantRates,1)
            
            rate = instantRates(t).rate((flags(t,1):flags(t,2)));
            vel = feats.velocities(t).instantVelocity((flags(t,1):flags(t,2)));
            velBefore = feats.velocities(t).instantVelocity(1:(flags(t,1))-1);
            velAfter = feats.velocities(t).instantVelocity((flags(t,2)+1):end);
            rateBefore = instantRates(t).rate(1:(flags(t,1))-1);
            rateAfter = instantRates(t).rate((flags(t,2)+1):length(feats.velocities(t).instantVelocity));

            hold on
            %scatter(feats.velocities(t).instantVelocity,instantRates(t).rate(1:length(feats.velocities(t).instantVelocity)),5,'red','filled');
            scatter(vel,rate,3,'red','filled');
            hold on
            scatter(rateBefore,rateBefore,3,'green','filled');
            hold on
            scatter(velAfter,rateAfter,3,'blue','filled');
        end
        title(['velocity channel ' sprintf('%d',sUnits(j).channel)  ' unit ', sprintf('%d',sUnits(j).unit)]);
        
        %plot force
        subplot(2,5,j + 5);
        
        for t = 1:size(instantRates,1)
            rate = instantRates(t).rate(flags(t,1):flags(t,2));
            force = feats.forces(t).instantForce(flags(t,1):flags(t,2));
            %disp(length(instantRates(t).rate));
            %disp(length(feats.forces(t).instantForce));
            forceBefore = feats.forces(t).instantForce(1:(flags(t,1))-1);
            forceAfter =  feats.forces(t).instantForce((flags(t,2)+1):end);
            rateBefore = instantRates(t).rate(1:(flags(t,1))-1);
            rateAfter = instantRates(t).rate((flags(t,2)+1):length(feats.forces(t).instantForce));
            if i == 1 && j == 1
                sizes(1) = sizes(1) + length(rateBefore);
                sizes(2) = sizes(2) + length(rate) ;
                sizes(3) = sizes(3) + length(rateAfter);
            end
            %disp(length(rateExt));
            %disp(length(forceExt));

            hold on;
            %scatter(feats.forces(t).instantForce,instantRates(t).rate(1:length(feats.forces(t).instantForce)),5,'red','filled');
            scatter(force,rate,3,'red','filled');
            hold on
            scatter(forceBefore,rateBefore,3,'green','filled');
            hold on
            scatter(forceAfter,rateAfter,3,'blue','filled');
            
        end

        title(['force channel ' sprintf('%d',sUnits(j).channel)  ' unit ', sprintf('%d',sUnits(j).unit)]);

    end
    
end
%plot proportion of points before, during and after the "velocity bell"
figure();
pie(sizes);
colormap([0,1,0;1,0,0;0,0,1]);