function [NSP_index, lChan] = findLocalChannel(channel,elec_index,NSP1_mat,NSP2_mat)
%         
% findLocalChannel : give the indices of the NSP and the local channel for
%   the given channel
% 
% INPUT ARGUMENTS :
% 
% channel : index of the channel
% elec_index : index of the electrode array 
% NSP1_mat : reference mat file for the data recorded by NSP1
% NSP2_mat : reference mat file for the data recorded by NSP2
% 
% RETURN:
% 
% NSP_index : index of the NSP where is the channel is located (1 or 2)
% lChan : local index of the channel in the NSP


lChan = 0;
    NSP_index = 0;
    for i = 1:128
        if strcmp(char(NSP1_mat.NEV.ElectrodesInfo(i).ElectrodeLabel),strcat('elec',num2str(elec_index),'-chan',num2str(channel)))
            lChan = i;
            NSP_index = 1;
            break
        end  
        if strcmp(char(NSP2_mat.NEV.ElectrodesInfo(i).ElectrodeLabel),strcat('elec',num2str(elec_index),'-chan',num2str(channel)))
            lChan = i;
            NSP_index = 2;
            break
        end
    end
end

