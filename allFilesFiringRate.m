
function rates = allFilesFiringRate()
%calculate maximum rate for files 13 to 35
all_rates1 = [];
for i = 13:34
    if i ~=19       %event file 19 is empty
        if isempty(all_rates1)
           all_rates1 = firingRateGrid(sprintf('%02d',i),'elec2');
        else
           all_rates1 = [all_rates1, firingRateGrid(sprintf('%02d',i),'elec2')];
        end
    end
end
%{
all_rates2 = [];
for i = 10:11
    if i ~=19       %event file 19 is empty
        if isempty(all_rates2)
           all_rates2 = firingRateGrid(sprintf('%02d',i),'elec6');
        else
           all_rates2 = [all_rates2, firingRateGrid(sprintf('%02d',i),'elec6')];
        end
    end
end
%}
hist(max(all_rates1,[],2),100);
disp(mean(max(all_rates1,[],2)));
rates = max(all_rates1,[],2);
%disp(mean(max(all_rates2,[],2)));
%hist(max(all_rates1,[],2));


