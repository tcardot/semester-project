function output=ricampiona(input,n)

% Questa funzione prende un vettore (input) e lo ricampiona sulla base del
% numedo di campioni richiesti (n)

c=length(input);
asc1=1:c;
asc2=0:c/n:c;
output=spline(asc1,input,asc2); %utilizzando la spline invece di resample si eliminano gli effetti di bordo

output(1)=[];
