clear all;
close all;
clc;

%% Set up the location of the data
%   Need to define where the data is kept on this computer, which monkey
% you want, and which date you want
% root_dir = '/Users/mattperich/Dropbox/Research/Data/';
root_dir = '/Volumes/IC STROKE DATA/';

% monkey = 'Cersei';
% the_date = '20180615';
% file_prefix = ['ReTh_' the_date '_' monkey '_Brain'];

monkey_dir = 'REY';
monkey = 'Rey';
the_date = '20190115';
file_prefix = ['ReTh_' the_date monkey 'NSP1'];


%% set up the necessary info
data_dir = [fullfile(root_dir, monkey_dir, 'EXPERIMENT', the_date, 'BLACKROCK_WIRED') filesep];

%% perform artifacts removal in all files and save them back cleaned
NEVlist = dir([data_dir file_prefix '*.nev']);

for iFile = 1:length(NEVlist)
    disp(['File ' num2str(iFile) ' of ' num2str(length(NEVlist))]);
    file_name = [NEVlist(iFile).folder filesep NEVlist(iFile).name];
    file_name_save = ['AR_' NEVlist(iFile).name];
    NEV = openNEVCervical(file_name,'nosave');
    NEV_clean = artifact_removal(NEV,10,0.0005,1);
    saveNEVSpikesCervical(NEV_clean,data_dir,file_name_save);
    
    clear NEV NEV_clean file_name_save file_name
end

