%old function to compute the instantaneous firing rate of one
%multi-electrode array

function max_rate = firingRateGrid(record_number,electrode_array)
%set directories
spikes_dir = 'D:\JYN\20190516';
events_dir = 'D:\JYN\20190516\Event';
vicon_dir = 'D:\JYN\20190516\VICON';
%load files

%{
mat_s1 = load(fullfile(spikes_dir,'BLACKROCK_NSP1','ReTh_20190516JynBrainNSP1001'));
mat_s2 = load(fullfile(spikes_dir,'BLACKROCK_NSP2','ReTh_20190516JynBrainNSP2001'));
mat_e = load(fullfile(events_dir, '20190516_Jyn_Brain01_Struct'));
mat_v = load(fullfile(vicon_dir, '20190516_Jyn_Brain01'));
%}

mat_s1 = load(fullfile(spikes_dir,'BLACKROCK_NSP1',strcat('ReTh_20190516JynBrainNSP10',record_number)));
mat_s2 = load(fullfile(spikes_dir,'BLACKROCK_NSP2',strcat('ReTh_20190516JynBrainNSP20',record_number)));
mat_e = load(fullfile(events_dir, strcat('20190516_Jyn_Brain',record_number,'_Struct')));
mat_v = load(fullfile(vicon_dir, strcat('20190516_Jyn_Brain',record_number)));

spikes1 = mat_s1.NEV.Data.Spikes.TimeStamp;
elec1 = mat_s1.NEV.Data.Spikes.Electrode;
unit1 = mat_s1.NEV.Data.Spikes.Unit;

spikes2 = mat_s2.NEV.Data.Spikes.TimeStamp;
elec2 = mat_s2.NEV.Data.Spikes.Electrode;
unit2 = mat_s2.NEV.Data.Spikes.Unit;

spike_res = mat_s1.NEV.MetaTags.SampleRes;
sync_res = mat_v.analog.framerate;
event_res = mat_v.kinematic.framerate;
trials= mat_e.structTrials;
begin = zeros(length(trials),1);
lightOff = zeros(length(trials),1);

maxUnit = 5;


for t = 1:length(trials)
    begin(t) = floor(trials(t).beginningOfMovement*spike_res/event_res);
    lightOff(t) = floor(trials(t).LightOFF* spike_res/event_res);
end

%sync
[~,max_nsp1_index] = max(diff(mat_v.analog.data(:,1)));
spikes1 = spikes1 - floor(max_nsp1_index* spike_res/sync_res);
[~,max_nsp2_index] = max(diff(mat_v.analog.data(:,9)));
spikes2 = spikes2 - floor(max_nsp2_index* spike_res/sync_res);


time_window1 = 0.01  * spike_res;

%select channels 
%electrode = 'elec4';
electrode = electrode_array;
elec_NSP1 = [];
elec_NSP2 = [];

for t = 1:128
    if ~isempty(strfind(char(mat_s1.NEV.ElectrodesInfo(t).ElectrodeLabel),electrode))
        elec_NSP1 = [elec_NSP1;t];
    end
    if ~isempty(strfind(char(mat_s2.NEV.ElectrodesInfo(t).ElectrodeLabel),electrode))
        elec_NSP2 = [elec_NSP2;t];  
    end
end

%compute histogram

avg_rate = [];

for t = 1:length(trials)
    t_grid = singlemove_grid(begin(t),lightOff(t),time_window1);
    %imagesc(t_grid);
    %colorbar;
    duration = double(time_window1*size(t_grid,2))/double(spike_res);
    %disp(duration);
    t_avg_rate = double(sum(t_grid,2))/duration;
    if isempty(avg_rate)
        avg_rate = t_avg_rate;
    else
        avg_rate = [avg_rate,t_avg_rate];
    end
end

%hist(trials_peak);
%disp(avg_rate);
%hist(max(avg_rate,[],2),20);
%sdisp(mean(max(avg_rate,[],2)));
max_rate = max(avg_rate,[],2);

%disp(max(max(graph)));
%disp(max(max([grid_NSP1(:,:,1);grid_NSP1(:,:,2)])));
%imagesc(grid_NSP1(:,:,1));
%graph1 = singlemove_grid(begin(1),lightOff(1),time_window1);
%graph2 = singlemove_grid(begin(3),lightOff(3),time_window1);
%imagesc(graph1);
%colorbar;
%figure();
%imagesc(graph2);
%colormap jet;
%colorbar;

function graph = singlemove_grid(start,stop,time_window)
    %select spikes
    start_time = start;
    timeSubdiv = ceil(double((stop-start)/double(time_window)));
    end_time = start + time_window * timeSubdiv;
    %end_time = start + time_window *ceil(double((stop-start)/double(time_window)));
    %timeSubdiv = double(((stop - start)/time_window));
    
    s_spikes1 = spikes1(spikes1 <= end_time & spikes1 >= start_time);
    s_elec1 = elec1(spikes1 <= end_time & spikes1 >= start_time);
    s_unit1 = unit1(spikes1 <= end_time & spikes1 >= start_time);

    s_spikes2 = spikes2(spikes2 <= end_time & spikes2 >= start_time);
    s_elec2 = elec2(spikes2 <= end_time & spikes2 >= start_time);
    s_unit2 = unit2(spikes2 <= end_time & spikes2 >= start_time);
    
    %init grids
    grid_NSP1 = zeros(length(elec_NSP1),timeSubdiv,maxUnit +1);
    grid_NSP2 = zeros(length(elec_NSP2),timeSubdiv,maxUnit +1);

    %count spikes for NSP1 and NSP2
    for i = 1:length(s_spikes1)
        if ismember(s_elec1(i),elec_NSP1)
            unit_index = maxUnit +1;
            if s_unit1(i) >=1 && s_unit1(i) <= maxUnit
                unit_index = s_unit1(i);
            end
            index = floor(timeSubdiv*double(s_spikes1(i) - start_time)/double(end_time - start_time +1)) +1;
            grid_NSP1(elec_NSP1 ==s_elec1(i),index,unit_index) = grid_NSP1(elec_NSP1 ==s_elec1(i),index,unit_index) +1;
        end

    end

    for i = 1:length(s_spikes2)
        if ismember(s_elec2(i),elec_NSP2)
            unit_index = maxUnit + 1;
            if s_unit2(i) >= 1 && s_unit2(i) <= maxUnit
                unit_index = s_unit2(i);
            end
            index = floor(timeSubdiv*double(s_spikes2(i) - start_time)/double(end_time - start_time +1)) +1;
            grid_NSP2(elec_NSP2 ==s_elec2(i),index,unit_index) = grid_NSP2(elec_NSP2 ==s_elec2(i),index,unit_index) +1;
        end

    end

    %add non empty units to the graph
    graph = [];
    for i = 1:length(elec_NSP1)
        nUnits = mat_s1.NEV.ElectrodesInfo(elec_NSP1(i)).Units;
        for j = 1:nUnits
            graph = [graph; grid_NSP1(i,:,j)];
        end
    end
    for i = 1:length(elec_NSP2)
        nUnits = mat_s2.NEV.ElectrodesInfo(elec_NSP2(i)).Units;
        for j = 1:nUnits
            graph = [graph; grid_NSP2(i,:,j)];
        end
    end
    imagesc(graph);
    colorbar;
end

end


