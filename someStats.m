rates1 = computeElectrodeFiringRate(arraysBeforeLesion.array1);
rates2 = computeElectrodeFiringRate(arraysBeforeLesion.array2);
rates3 = computeElectrodeFiringRate(arraysBeforeLesion.array3);
rates4 = computeElectrodeFiringRate(arraysBeforeLesion.array4);
rates5 = computeElectrodeFiringRate(arraysBeforeLesion.array5);
rates6 = computeElectrodeFiringRate(arraysBeforeLesion.array6);

array1 = arraysBeforeLesion.array1(rates1 > 1);
array2 = arraysBeforeLesion.array2(rates2 > 1);
array3 = arraysBeforeLesion.array3(rates3 > 1);
array4 = arraysBeforeLesion.array4(rates4 > 1);
array5 = arraysBeforeLesion.array5(rates5 > 1);
array6 = arraysBeforeLesion.array6(rates6 > 1);

nElec1 = 48;
nElec2 = 48;
nElec3 = 32;
nElec4 = 48;
nElec5 = 48;
nElec6 = 32;
disp('before lesion');
disp(100*double(size(array1,1))/double(nElec1));
disp(100*double(size(array2,1))/double(nElec2));
disp(100*double(size(array3,1))/double(nElec3));
disp(100*double(size(array4,1))/double(nElec4));
disp(100*double(size(array5,1))/double(nElec5));
disp(100*double(size(array6,1))/double(nElec6));

rates1 = computeElectrodeFiringRate(arraysAfterLesion.array1);
rates2 = computeElectrodeFiringRate(arraysAfterLesion.array2);
rates3 = computeElectrodeFiringRate(arraysAfterLesion.array3);
rates4 = computeElectrodeFiringRate(arraysAfterLesion.array4);
rates5 = computeElectrodeFiringRate(arraysAfterLesion.array5);
rates6 = computeElectrodeFiringRate(arraysAfterLesion.array6);

array1 = arraysAfterLesion.array1(rates1 > 1);
array2 = arraysAfterLesion.array2(rates2 > 1);
array3 = arraysAfterLesion.array3(rates3 > 1);
array4 = arraysAfterLesion.array4(rates4 > 1);
array5 = arraysAfterLesion.array5(rates5 > 1);
array6 = arraysAfterLesion.array6(rates6 > 1);

nElec1 = 48;
nElec2 = 48;
nElec3 = 32;
nElec4 = 48;
nElec5 = 48;
nElec6 = 32;

disp('after lesion');
disp(100*double(size(array1,1))/double(nElec1));
disp(100*double(size(array2,1))/double(nElec2));
disp(100*double(size(array3,1))/double(nElec3));
disp(100*double(size(array4,1))/double(nElec4));
disp(100*double(size(array5,1))/double(nElec5));
disp(100*double(size(array6,1))/double(nElec6));