%compute the flags used to separate the movement in three parts : before
%the velocity bell, during the velocity bell and after the velocity bell



feats20190516 = load('feats20190516.mat');
feats20190604 = load('feats20190604.mat');
feats20190516 = feats20190516.feats20190516;
feats20190604 = feats20190604.feats20190604;

avgLength = 30;
coeffs = ones(1,avgLength)/avgLength;
windowLeft = 30;
windowRight = 30;
figure();
moveFlags20190516 = zeros(size(feats20190516.velocities,1),2);
moveFlags20190604 = zeros(size(feats20190604.velocities,1),2);
for i = 1:size(feats20190604.velocities,1)
    
    vel = feats20190604.velocities(i).instantVelocity;
    fVel = filter(coeffs,1,vel);
    [maxVel,maxVelIndex] = max(fVel);
    fVel(1:maxVelIndex-windowLeft) = 0;
    fVel(maxVelIndex + windowRight:end) = 0;
    moveFlags20190604(i,:) = [maxVelIndex-windowLeft,maxVelIndex+windowRight];
    hold on
    plot(fVel,'blue');

end
figure()
%for i = 1:size(feats20190516.velocities,1)
for i = 1:size(feats20190516.velocities,1)
    vel = feats20190516.velocities(i).instantVelocity;
    fVel = filter(coeffs,1,vel);
    [maxVel,maxVelIndex] = max(fVel);
    disp(maxVelIndex);
    fVel(1:maxVelIndex-windowLeft) = 0;
    fVel(maxVelIndex + windowRight:end) = 0;
    moveFlags20190516(i,:) = [maxVelIndex-windowLeft,maxVelIndex+windowRight];
    hold on
    plot(fVel,'red');

end