function [wscores,iScores] = computeStabilityScore(arrayNumber)
% 
% computeStabilityScore : compute the waveform and ISIH score of units in
% one multi-electrode array
% 
% 
% 


%select units with max firing rate > 1Hz
arraysBeforeLesion = load('arraysBeforelesion.mat');
arraysAfterLesion = load('arraysAfterLesion.mat');
 

rates1 = computeElectrodeFiringRate(arraysBeforeLesion.array1);
rates2 = computeElectrodeFiringRate(arraysBeforeLesion.array2);
rates3 = computeElectrodeFiringRate(arraysBeforeLesion.array3);
rates4 = computeElectrodeFiringRate(arraysBeforeLesion.array4);
rates5 = computeElectrodeFiringRate(arraysBeforeLesion.array5);
rates6 = computeElectrodeFiringRate(arraysBeforeLesion.array6);
%{
array1 = arraysBeforeLesion.array1;
array2 = arraysBeforeLesion.array2;
array3 = arraysBeforeLesion.array3;
array4 = arraysBeforeLesion.array4;
array5 = arraysBeforeLesion.array5;
array6 = arraysBeforeLesion.array6;
%}
sElec = [];
ratesA = [];
ratesB = [];
switch arrayNumber
    case 1
        ratesB = computeEleectrodeFiringRate(arraysBeforeLesion.array1);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array1);
        sElec = arraysBeforeLesion.array1(ratesB > 2 & ratesA > 2);
    case 2
        ratesB = computeElectrodeFiringRate(arraysBeforeLesion.array2);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array2);
        sElec = arraysBeforeLesion.array2(ratesB > 2 & ratesA > 2);
    case 3
        ratesB = computeElectrodeFiringRate(arraysBeforeLesion.array3);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array3);
        sElec = arraysBeforeLesion.array3(ratesB > 2 & ratesA > 2);
    case 4
        ratesB = computeElectrodeFiringRate(arraysBeforeLesion.array4);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array4);
        sElec = arraysBeforeLesion.array4(ratesB > 2 & ratesA > 2);
    case 5
        ratesB = computeElectrodeFiringRate(arraysBeforeLesion.array5);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array5);
        sElec = arraysBeforeLesion.array5(ratesB > 2 & ratesA > 2);
    case 6
        ratesB = computeElectrodeFiringRate(arraysBeforeLesion.array6);
        ratesA = computeElectrodeFiringRate(arraysAfterLesion.array6);
        sElec = arraysBeforeLesion.array6(ratesB > 1.5 & ratesA > 1.5);
end
[muB,sigB,muA,sigA,pcorr] = computeUnitStability(sElec(1).unit,sElec(1).channel,arrayNumber);
scores = [muB,sigB,muA,sigA,pcorr];
for i = 2:length(sElec);
    [t_muB,t_sigB,t_muA,t_sigA,t_pcorr] = computeUnitStability(sElec(i).unit,sElec(i).channel,arrayNumber);
    scores = [scores;t_muB,t_sigB,t_muA,t_sigA,t_pcorr];
end
%scores = arrayfun(@(x)computeUnitStability(x.unit,x.channel,2),sElec);
%disp(scores);
sigmas = std(scores,0,1);
sigmas(sigmas == 0) = 1; %replace zeros by ones to avoid division by zero
%disp(sigmas);
isiscores = [scores(:,3)- scores(:,1),scores(:,4)-scores(:,2)];
isiscores = [isiscores(:,1)/sigmas(1),isiscores(:,2)/sigmas(2)];
isiscores = isiscores.^2;
iScores = sqrt(sum(isiscores,2));
iScores = log(iScores);
%disp(iScores);

wscores = atanh(scores(:,5));
scatter(wscores,iScores,'filled');
%disp(arrayfun(@(x) x.channel,sElec(wscores < 3.5)));
[~,sortedIndices] = sort(wscores);
%disp(arrayfun(@(x) x.channel, sElec(sortedIndices)));
end