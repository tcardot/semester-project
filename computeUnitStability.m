function [muB,sigB,muA,sigA,pcorr] = computeUnitStability(unit,channel,electrode)
% 
% computeUnitStability : compute the parameters used in the stability score
% of one unit for the day before the lesion and the day after
% 
% RETURN
% 
% muB,sigB : parameters of the ISIH before the lesion
% mA,sigA : parameters of the ISIH after the lesion
%pcorr : Pearson's correlation coefficient between the two average
%waveforms
% 
% INPUT ARGUMENTS
% 
% unit : index of the unit
% channel : channel of the unit
% electrode : index of the multi-electode array containing the channel



arraysBeforeLesion = load('arraysBeforelesion.mat');
arraysAfterLesion = load('arraysAfterLesion.mat');
bArray = {};
aArray = {};
spikeRes = 30000;
switch electrode
    case 1
        bArray = arraysBeforeLesion.array1;
        aArray = arraysAfterLesion.array1;
    case 2
        bArray = arraysBeforeLesion.array2;
        aArray = arraysAfterLesion.array2;
    case 3
        bArray = arraysBeforeLesion.array3;
        aArray = arraysAfterLesion.array3;
    case 4
        bArray = arraysBeforeLesion.array4;
        aArray = arraysAfterLesion.array4; 
    case 5
        bArray = arraysBeforeLesion.array5;
        aArray = arraysAfterLesion.array5;  
    case 6
        bArray = arraysBeforeLesion.array6;
        aArray = arraysAfterLesion.array6;
end
bSpikes = bArray(arrayfun(@(x) x.channel,bArray) == channel & arrayfun(@(x) x.unit,bArray) == unit).spikes;
aSpikes = aArray(arrayfun(@(x) x.channel,aArray) == channel & arrayfun(@(x) x.unit,aArray) == unit).spikes;

%compute average waveforms
aWaveforms = [];
bWaveforms = [];
for i = 1:size(aSpikes,1)
    if ~isempty(aSpikes(i).waveforms)
        if isempty(aWaveforms)
            aWaveforms = aSpikes(i).waveforms;
        else
            aWaveforms = [aWaveforms, aSpikes(i).waveforms];
        end
    end
end
for i = 1:size(bSpikes,1)
    if ~isempty(bSpikes(i).waveforms)
        if isempty(bWaveforms)
            bWaveforms = bSpikes(i).waveforms;
        else
            bWaveforms = [bWaveforms, bSpikes(i).waveforms];
        end
    end
end

avgwfB = mean(bWaveforms,2);
avgwfA = mean(aWaveforms,2);

%{
%figure();
subplot(2,3,1);
plot(avgwfB);
title('before lesion');
%figure();
subplot(2,3,2);
plot(avgwfA);
title('after lesion');
%figure();
subplot(2,3,3);
plot(avgwfB-avgwfA);
%}
corrmat = corrcoef(avgwfB,avgwfA);
pcorr = corrmat(2,1);

%compute isi's
isiB = [];
for i = 1: size(bSpikes,1)
    t_isi = diff(double(bSpikes(i).timeStamps) /double(spikeRes));
    if ~isempty(t_isi)   
        if isempty(isiB)
            isiB = t_isi;
        else
            isiB = [isiB,t_isi];
        end
    end
end

isiA = [];
for i = 1: size(aSpikes,1)
    t_isi = diff(double(aSpikes(i).timeStamps) /double(spikeRes));
    if ~isempty(t_isi)   
        if isempty(isiA)
            isiA = t_isi;
        else
            isiA = [isiA,t_isi];
        end
    end
end

pdB = fitdist(isiB.','lognormal');
pdA = fitdist(isiA.','lognormal');

%[label, model, llh] = mixGaussEm(log(isiB),4);


muB = mean(pdB);
muA =mean(pdA);
sigB = std(pdB);
sigA = std(pdA);


%figure()

%{
subplot(2,3,4);
hist(log(isiB),100);
title('before lesion');
%figure()
subplot(2,3,5);
hist(log(isiA),100);
title('after lesion');
figure();
plot(1:0.25:48,spline(1:48,avgwfB,1:0.25:48));
%}

end
