
%rename_electrodes : change the collum 'ElectrodeLabel' in the NEV mats
%with name channel names from the xls sheet
%the format is 'elec[array_number]-chan[channel_number]'
%elec_number : from 1 to 6
%channel_number : from 1 to 256 , NO zero is added for padding

%load xlsm sheet
%[NUM, TXT, RAW] = xlsread('D:\JYN\SN 4566-000004 array 1030-24, -25, -27, -29-1, -29-2, 1029-8-saved.xlsm',3);
%[NUM, TXT, RAW] = xlsread('D:\JYN\array-organization-corrected.xlsm',3);
elec_mat = load('D:\JYN\ElectrodesOverNSP.mat');
disp(elec_mat.NSP1);
%load .mat files to be renamed

data_dir = 'D:\JYN\20190604\BLACKROCK_NSP1';
all_files = dir(fullfile(data_dir,'*ReTh_20190604JynBrainInjuryNSP10*.mat'));
%disp(all_files(2).name);
for j = 1:length(all_files)
    Nev_mat = load(fullfile(data_dir,all_files(j).name));
    for i = 1:128
       % Nev_mat.NEV.ElectrodesInfo(i).ElectrodeLabel = RAW(2+i,4);
       Nev_mat.NEV.ElectrodesInfo(i).ElectrodeLabel = elec_mat.NSP1(i);
    end 
    save(fullfile(data_dir,all_files(j).name),'-struct', 'Nev_mat');
end


%disp(length(Nev_mat.NEV.ElectrodesInfo));
%disp( T(:,1));

