function electrode_struct = extractElectrode(elec_number,day,record_indices)

% extractElectrode : extract data from desired electrode array
%
% the data is stored in a array struct(each line represent a unit) with the following fields :
%   spikes : structure containing the spikes data
%   channel : index of the channel
%   unit : index of the unit recorded by the channel
% the 'spikes' is an array struct, each line represent one grasping task, 
%  it has the following fields :

%   duration : duration of the grasping movement, from
%   'beginningOfMovement' to  'LightOff'

%   waveforms : array containting the waveforms of the spikes, each collum
%    is waveform of length 48
%   timestamps : time at which the spikes occurs, in seconds
%  
% 
%INPUT ARGUMENTS :
%elec number :int , array number (1,2,...,6)
%day: char vector, day of recording ('yyyy.mm.dd')
%record_indices : int vector , indices of the recordings to be extracted


%load  NSP, VICON and Event matrices
day_dir = fullfile('D:','JYN',day);
NSP1_dir = fullfile(day_dir,'BLACKROCK_NSP1');
NSP2_dir = fullfile(day_dir,'BLACKROCK_NSP2');
event_dir = fullfile(day_dir,'Event');
vicon_dir = fullfile(day_dir,'VICON');

NSP1_mats = [];
NSP2_mats = [];
event_mats = [];
vicon_mats = [];
for i = 1:length(record_indices)
    if isempty(NSP1_mats)
        NSP1_mats = load(fullfile(NSP1_dir,strcat('ReTh_',day,'JynBrainNSP10',sprintf('%02d',record_indices(i)),'.mat')));
    else
        NSP1_mats = [NSP1_mats;load(fullfile(NSP1_dir,strcat('ReTh_',day,'JynBrainNSP10',sprintf('%02d',record_indices(i)),'.mat')))];
    end
    
    if isempty(NSP2_mats)
        NSP2_mats = load(fullfile(NSP2_dir,strcat('ReTh_',day,'JynBrainNSP20',sprintf('%02d',record_indices(i)),'.mat')));
    else
        NSP2_mats = [NSP2_mats;load(fullfile(NSP2_dir,strcat('ReTh_',day,'JynBrainNSP20',sprintf('%02d',record_indices(i)))))];
    end
    
    if isempty(vicon_mats)
        vicon_mats = load(fullfile(vicon_dir,strcat(day,'_Jyn_Brain', sprintf('%02d',record_indices(i)),'.mat'))); 
    else
        vicon_mats = [vicon_mats;load(fullfile(vicon_dir,strcat(day,'_Jyn_Brain', sprintf('%02d',record_indices(i)))))];
    end 
    if isempty(event_mats)
        event_mats = load(fullfile(event_dir,strcat(day,'_Jyn_Brain',sprintf('%02d',record_indices(i)),'_Struct.mat')));
    else
        event_mats = [event_mats;load(fullfile(event_dir,strcat(day,'_Jyn_Brain',sprintf('%02d',record_indices(i)),'_Struct.mat')))];
    end
end 

%electrodes in the selected array
channels = [];
if elec_number == 1 
    channels = 1:48; 
elseif elec_number == 2
    channels = 49:96;
elseif elec_number == 3
    channels = 97:144;
elseif elec_number == 4
    channels = 145:176;
elseif elec_number == 5
    channels = 177:224;
else
    channels = 225:256;
end
    
units = zeros(length(channels),1);
for i = 1: length(channels)
    [NSP_index, lChan] = findLocalChannel(channels(i),elec_number,NSP1_mats(1),NSP2_mats(1));
    if NSP_index == 1
        units(i) = NSP1_mats(1).NEV.ElectrodesInfo(lChan).Units;
    else
        units(i) = NSP2_mats(1).NEV.ElectrodesInfo(lChan).Units;
    end

end

electrode_struct = {};
%append units to the structure
for i = 1:length(channels)
    for j = 1:units(i)
        
        t_struct.channel= channels(i);
        t_struct.unit = j;
        t_struct.spikes = extractUnit(j,channels(i),elec_number,NSP1_mats,NSP2_mats,event_mats,vicon_mats);
        
        if isempty(electrode_struct)
            electrode_struct = t_struct;
        else
            electrode_struct = [electrode_struct; t_struct];
        end
    end
end
end

