function pValue = permutationTest(unitA,unitB)

%load arrays
%{
arrays20190516 = load('arrays20190516.mat','arrays20190516');
arrays20190516 = arrays20190516.arrays20190516;
arrays20190604 = load('arrays20190604.mat','arrays20190604');
arrays20190604 = arrays20190604.arrays20190604;
%}
waveformsA = [];
for i = 1: size(unitA.spikes,1)
    tW = unitA.spikes(i).waveforms;
    if isempty(waveformsA)
        waveformsA = tW;
    else
        if ~isempty(tW)
            waveformsA = [waveformsA,tW];
        end
    end
end

waveformsB = [];
for i = 1: size(unitB.spikes,1)
    tW = unitB.spikes(i).waveforms;
    if isempty(waveformsB)
        waveformsB = tW;
    else
        if ~isempty(tW)
            waveformsB = [waveformsB,tW];
        end
    end
end

%compute statistic for original sets
mA = mean(waveformsA,2);
mB = mean(waveformsB,2);
To = norm(mA-mB);

%{
figure();
for i = 1:size(waveformsB,2)  
    hold on
    plot(waveformsB(:,i),'red');
end
for i = 1:size(waveformsA,2)  
    hold on
    plot(waveformsA(:,i),'blue');
end

plot(mA,'green');
hold on 
plot(mB,'yellow');
%}

%generate permutation sets
nA = size(waveformsA,2);
nB = size(waveformsB,2);


totalSet = [waveformsA,waveformsB];

nIter = 1000;
permT = zeros(nIter,1);

figure();

for i = 1:nIter
    randIndices = randperm(nA+nB);
    setA = totalSet(:,randIndices(1:nA));
    setB = totalSet(:,randIndices(nA+1:nA + nB));
    tmA = mean(setA,2);
    tmB = mean(setB,2);
    permT(i) = norm(tmA-tmB);
    hold on
    plot(tmA);
    hold on
    plot(tmB);
end

hold on
plot(mA,'green');
hold on 
plot(mB,'red');
%pValue = double(length(permT(permT > To)))/nIter;
pValue = length(permT(permT > To));
disp(permT);
%disp(To);
%disp(max(permT));
end

