stabilityData = load('stabilityData','stabilityData');
stabilityData = stabilityData.stabilityData;

compIndices = nchoosek(1:size(stabilityData,1),2);
pValuesNeg = zeros(size(compIndices,1),1);

for j = 1: size(compIndices,1)
    %waveforms
    waveformsA = stabilityData(compIndices(j,1)).data.waveforms;
    waveformsB = stabilityData(compIndices(j,2)).data.waveforms;

    %compute original statistic 
    mA = mean(waveformsA,2);
    mB = mean(waveformsB,2);
    To = norm(mA-mB);
    nA = size(waveformsA,2);
    nB = size(waveformsB,2);
    totalSet = [waveformsA, waveformsB];
    %permute sets
    nIter = 1000;
    permT = zeros(nIter,1);
    for t = 1:nIter      
        randIndices = randperm(nA+nB);
        setA = totalSet(:,randIndices(1:nA));
        setB = totalSet(:,randIndices(nA+1:nA + nB));
        tmA = mean(setA,2);
        tmB = mean(setB,2);
        permT(t) = norm(tmA-tmB);

    end
    pValuesNeg(j) = double(length(permT(permT > To) )+1)/double((length(permT)+1));

end
hist(pValuesNeg,50);
save('pValuesNeg','-struct','pValuesNeg');