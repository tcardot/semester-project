clear all;
close all;
clc;

%% Set up the location of the data
%   Need to define where the data is kept on this computer, which monkey
% you want, and which date you want
% root_dir = '/Users/mattperich/Dropbox/Research/Data/';
root_dir = '/Volumes/IC STROKE DATA/';

% monkey = 'Cersei';
% the_date = '20180615';
% file_prefix = ['ReTh_' the_date '_' monkey '_Brain'];

monkey_dir = 'REY';
monkey = 'Rey';
the_date = '20190212';
file_prefix = ['ReTh_' the_date monkey 'NSP1'];


%% set up the necessary info
data_dir = [fullfile(root_dir, monkey_dir, 'EXPERIMENT', the_date, 'BLACKROCK_WIRED') filesep];


%% First, rethreshold all the .NS5 files and save them as new .NEVs
%   Each .NS5 file in the destination folder will be rethresholded and
% coverted to a new .NEV file with 'ReTh_' added to the beginning.
%   e.g., 'ReTh_Sansa_20180924_Brain001.nev'
rethresholdAllNEVpopulation;