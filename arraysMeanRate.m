
arraysBefore = load('arrays20190516');
arraysAfter = load('arrays20190604');

arraysBefore = arraysBefore.arrays20190516;
arraysAfter = arraysAfter.arrays20190604;


rates = zeros(6,2);
for i = 1:6
    
    rates(i,:) = [mean(computeElectrodeFiringRate(arraysBefore(i).array)),mean(computeElectrodeFiringRate(arraysAfter(i).array))];
end

