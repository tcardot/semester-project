%CompareNeurons : compute the stability scores between 2019.05.16 and
%2019.06.04
%
%arrays contain only spikes occuring during the grasping task
%
%
%


arraysBefore = load('arraysBeforeLesion');
arraysAfter = load('arraysAfterLesion');

stdMu = load('isiStdMu');
stdMu = stdMu.stdMu;
stdSigma = load('isiStdSigma');
stdSigma = stdSigma.stdSigma;
%concatenate units


unitBeforeSet = [arraysBefore.array1;arraysBefore.array2;arraysBefore.array3;arraysBefore.array4;arraysBefore.array5;arraysBefore.array6];
unitAfterSet = [arraysAfter.array1;arraysAfter.array2;arraysAfter.array3;arraysAfter.array4;arraysAfter.array5;arraysAfter.array6];
%compute rates and keep only unit with FR > minRate
ratesBefore = computeElectrodeFiringRate(unitBeforeSet);
ratesAfter = computeElectrodeFiringRate(unitAfterSet);
minRate = 2;
unitBeforeSet = unitBeforeSet(ratesBefore >= minRate & ratesAfter >= minRate);
unitAfterSet = unitAfterSet(ratesBefore >= minRate & ratesAfter >= minRate);

%compute features for the scores
[beforeMus,beforeSigmas,beforeWf] = UnitStabilityFeatures(unitBeforeSet(1));
[afterMus,afterSigmas,afterWf] = UnitStabilityFeatures(unitAfterSet(1));
for i = 2:size(unitBeforeSet,1)
    [tbMu,tbSigma,tbWf] = UnitStabilityFeatures(unitBeforeSet(i));
    [taMu,taSigma,taWf] = UnitStabilityFeatures(unitAfterSet(i));
    beforeMus = [beforeMus;tbMu];
    afterMus = [afterMus;taMu];
    beforeSigmas = [beforeSigmas;tbSigma];
    afterSigmas = [afterSigmas;taSigma];
    beforeWf = [beforeWf,tbWf];
    afterWf = [afterWf,taWf];
end

isihScores = zeros(length(beforeMus),1);
%compute scores
for i = 1:length(beforeMus)
    isihScores(i) = sqrt(((beforeMus(i) - afterMus(i))).^2)/stdMu^2 + ...
        ((beforeSigmas(i) -afterSigmas(i).^2)/stdSigma^2);
end

%isihScores = sqrt( ((beforeMus-afterMus)/stdMu).^2 + ((beforeSigmas-afterSigmas/stdSigma)).^2);
isihScores = log(isihScores);


wfScores = zeros(size(beforeWf,2),1);
for i = 1:size(beforeWf,2)
    
    corrmat = corrcoef(beforeWf(:,i),afterWf(:,i));
    wfScores(i) = corrmat(2,1);
end

wfScores = atanh(wfScores);


