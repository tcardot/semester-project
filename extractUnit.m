
function unitStruct = extractUnit(unit,channel,electrode, NSP1_mats,NSP2_mats,event_mats, vicon_mats)

% extractUnit : extract data from desired unit
%
% the data is stored in a array struct(each line represent a unit) with the following fields :
%   spikes : structure containing the spikes data
%   channel : index of the channel
%   unit : index of the unit recorded by the channel
% the 'spikes' is an array struct, each line represent one grasping task, 
%  it has the following fields :

%   duration : duration of the grasping movement, from
%   'beginningOfMovement' to  'LightOff'

%   waveforms : array containting the waveforms of the spikes, each collum
%    is waveform of length 48
%   timestamps : time at which the spikes occurs, in seconds
%  
% 
%INPUT ARGUMENTS :
%unit : index of the unit
%channel : channel of the unit
%electrode: index of the the multi-electrode array
%NSP1_mats : mat files of the NSP1 recordings
%NSP2_mats : mat files of the NSP2 reocordings
%event_mats : mat files containing the timestamps of the events of the
%trials( beginning of movement,grapsing...)
%vicon_mats : mat files of the vicon recordings
   [NSP_index, lChan] = findLocalChannel(channel,electrode,NSP1_mats(1),NSP2_mats(1));
   if NSP_index == 1
       NSP_mats = NSP1_mats;
   else
       NSP_mats = NSP2_mats; 
   end
   unitStruct = extractSingleRecord(NSP_mats(1),event_mats(1),vicon_mats(1),NSP_index,unit,lChan);
   
   for t = 2:length(event_mats)
       unitStruct = [unitStruct;extractSingleRecord(NSP_mats(t),event_mats(t),vicon_mats(t),NSP_index,unit,lChan)];
   end

    function record_struct = extractSingleRecord(NSP_mat,event_mat,VICON_mat,NSP_number,unit,lchannel)
        
        spike_res = NSP_mat.NEV.MetaTags.SampleRes;
        sync_res = VICON_mat.analog.framerate;
        event_res = VICON_mat.kinematic.framerate;
        %keep only spikes from desired unit
        timestamps = NSP_mat.NEV.Data.Spikes.TimeStamp(NSP_mat.NEV.Data.Spikes.Electrode == lchannel & NSP_mat.NEV.Data.Spikes.Unit == unit);
        waveforms = NSP_mat.NEV.Data.Spikes.Waveform(:,NSP_mat.NEV.Data.Spikes.Electrode == lchannel & NSP_mat.NEV.Data.Spikes.Unit == unit);
        %sync
        NSP_voltage_index = 9*(NSP_number -1) + 2 - NSP_number;
        [~,max_nsp_index] = max(diff(VICON_mat.analog.data(:,NSP_voltage_index)));
        timestamps = timestamps - floor(max_nsp_index * spike_res/sync_res);
        record_struct = {};
        %append trials to the structure
        for i = 1:length(event_mat.structTrials)
            begin = double(event_mat.structTrials(i).beginningOfMovement)* double(spike_res/event_res);
            stop = double(event_mat.structTrials(i).LightOFF) *double(spike_res/event_res);
            t_struct.timeStamps = double(timestamps(timestamps >= begin & timestamps <= stop) - begin)/double(spike_res); %time is relative to the beginning of the movement AND in seconds
            t_struct.waveforms = waveforms(:,timestamps >= begin & timestamps <=stop);
            t_struct.duration = double(stop -begin)/double(spike_res);
            if isempty(record_struct)
                record_struct = t_struct;
            else
                record_struct = [record_struct;t_struct];
            end
        end
    end

end

