function rates = computeElectrodeFiringRate(electrode_struct)
%
% computeElectrodeFiringRate: compute the maximum mean firing rate among
% the tasks for each unit in the array
% 
% 
% INPUT ARGUMENT:
% 
% electrode_struct : array struct containing the units
% 



nUnits = size(electrode_struct,1);
rates = zeros(nUnits,1);
for i = 1:length(rates)
    spikes = electrode_struct(i).spikes;
    rates(i) = max(arrayfun(@(x) length(x.timeStamps)/x.duration,spikes));
end

end