%
%
feats20190516 = load('feats20190516.mat');
feats20190604 = load('feats20190604.mat');
feats20190604xForce = load('feats20190604xForce');
feats20190604AbsForce = load('feats20190604AbsForce');
%structure is in another structure with same name because it was saved
%manually in matlab workspace
feats20190516 = feats20190516.feats20190516;
feats20190604 = feats20190604.feats20190604;
feats20190604xForce = feats20190604xForce.feats20190604xForce;
feats20190604AbsForce = feats20190604AbsForce.feats20190604AbsForce;

%choose structures to plot
featsA = feats20190604AbsForce;
featsB = feats20190604xForce;

figure();
%subplot(1,2,1);
for i = 1: size(featsA.forces,1)
    hold on
    scatter(featsA.velocities(i).instantVelocity, featsA.forces(i).instantForce,1,'red')

end
%{
subplot(1,2,2);
for i = 1: size(featsB.forces,1)
    hold on
    scatter(featsB.velocities(i).instantVelocity, featsB.forces(i).instantForce,1,'blue')

end
%}
