function instantRates = UnitInstantFiringRate(unit)
% 
% instantRates : compute the instantaneous firing rate of the unit, for
%  each movement. code in comments at the bottom plot the instant firing
%  rate of all movements and the average. Uncomment only when this function
%  is called for plotting
% 
% INPUT ARGUMENT :
% 
% unit : structure with the fields 'unit', 'channel' and 'spikes'
% 
% TIMESTAMPS MUST BE IN SECONDS
%
% RETURNS :
% 
% instantRates : array struct with the 
%

%time unit is in seconds
binSize = 0.01;
%instantRates = {};
instantRates = {};
%{
for i = 1:size(unit.spikes,1)
    nBins = ceil(unit.spikes(i).duration/binSize);
    instantRates(i).rate = zeros(nBins,1);
    for j = 1:length(unit.spikes(i).timeStamps)
        index = floor( unit.spikes(i).timeStamps(j)/binSize)+1;
        instantRates(i).rate(index) = instantRates(i).rate(index) + 1;    
    end
    instantRates(i).rate = instantRates(i).rate/unit.spikes(i).duration;
end
%}
for i = 1:size(unit.spikes,1)
    nBins = ceil(unit.spikes(i).duration/binSize);
    tRate = zeros(nBins,1);

    for j = 1:length(unit.spikes(i).timeStamps)
        
        if unit.spikes(i).timeStamps(j)/binSize == nBins   %avoid out of bound if timestamp is at the end
            index = nBins;
        else
            index = floor( unit.spikes(i).timeStamps(j)/binSize)+1;
        end
        tRate(index) = tRate(index) + 1;    
    end
    if isempty(instantRates)
        instantRates.rate = tRate/unit.spikes(i).duration;
    else
        tStruct.rate = tRate/unit.spikes(i).duration;
        instantRates = [instantRates;tStruct];
    end
end

%display rates as imagesc


maxSize = max(arrayfun(@(x) length(x.rate),instantRates));
image = zeros(size(instantRates,1),maxSize);
for t = 1:size(instantRates,1)
    image(t,1:length(instantRates(t).rate)) = instantRates(t).rate;
end
%{
figure()
subplot(2,1,1);
imagesc(mean(image,1));
subplot(2,1,2);
imagesc(image);
colorbar;
%disp(size(instantRates,1)); 
%}

