spikes_dir = 'D:\JYN\20190516';
events_dir = 'D:\JYN\20190516\Event';
vicon_dir = 'D:\JYN\20190516\VICON';
mat_s1 = load(fullfile(spikes_dir,'BLACKROCK_NSP1','ReTh_20190516JynBrainNSP1013'));
mat_s2 = load(fullfile(spikes_dir,'BLACKROCK_NSP2','ReTh_20190516JynBrainNSP2013'));
mat_e = load(fullfile(events_dir, '20190516_Jyn_Brain13_Struct'));
mat_v = load(fullfile(vicon_dir, '20190516_Jyn_Brain13'));
begin = mat_e.structTrials(2).beginningOfMovement * 300;
lightOff = mat_e.structTrials(2).LightOFF* 300;
%{
disp(begin);
disp(lightOff);
spikes1 = mat_s1.NEV.Data.Spikes.TimeStamp;
spikes2 = mat_s2.NEV.Data.Spikes.TimeStamp;
disp(length(spikes1(spikes1 <= lightOff & spikes1 >= begin)));
disp(length(spikes1(spikes2 <= lightOff & spikes2 >= begin)));
%}

%test_struct = extractUnit(1,2,1,[mat_s1],[mat_s2],[mat_e],[mat_v]);
test_struct = extractUnit(1,54,2,mat_s1,mat_s2,mat_e,mat_v); 
