%create one array struct containing all multi-electrode arrays and their
%name



day = '20190516';
indices = 13:34;
indices(7) = [];

arrays20190516(1).array = extractElectrode(1,day,indices);
arrays20190516(1).label = 'M1 LEFT';  

arrays20190516(2).array = extractElectrode(2,day,indices);
arrays20190516(2).label = 'S1 RIGHT';

arrays20190516(3).array = extractElectrode(3,day,indices);
arrays20190516(3).label = 'PMd RIGHT';

arrays20190516(4).array = extractElectrode(4,day,indices);
arrays20190516(4).label = 'PMv LEFT';

arrays20190516(5).array = extractElectrode(5,day,indices);
arrays20190516(5).label = 'M1 RIGHT';  

arrays20190516(6).array = extractElectrode(6,day,indices);
arrays20190516(6).label = 'PMv RIGHT';

save('arrays20190516.mat','arrays20190516');

