function [mu,sig,avgwf] =UnitStabilityFeatures(unit)
%
%UnitStabilityFeatures : compute the features used to compare units to
% assert stability
%
%INPUT ARGUMENT:
%
% unit : structure with the fields 'unit', 'channel' and 'spikes'
% 
% RETURNS:
% 
% mu : the mean of the lognormal distribution fitted to the ISIH
% sig : the std of the lognormal distribution fitted to the ISIH
% avgwf: the average waveform of the unit

waveforms = [];
spikeRes = 30000;
for i = 1:size(unit.spikes,1)
    if ~isempty(unit.spikes(i).waveforms)
        if isempty(waveforms)
            waveforms = unit.spikes(i).waveforms;
        else
            waveforms = [waveforms, unit.spikes(i).waveforms];
        end
    end
end
avgwf = mean(waveforms,2);

isi = [];
for i = 1: size(unit.spikes,1)
    t_isi = diff(double(unit.spikes(i).timeStamps) /double(spikeRes));
    if ~isempty(t_isi)   
        if isempty(isi)
            isi = t_isi;
        else
            isi = [isi,t_isi];
        end
    end
end
pd = fitdist(isi.','lognormal');
mu = mean(pd);
sig = std(pd);
end