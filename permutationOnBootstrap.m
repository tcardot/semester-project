
%apply permutation test on the true-positives set


stabilityData = load('stabilityData','stabilityData');
stabilityData = stabilityData.stabilityData;
%keep only units with more than 125 spikes

nSpikes = arrayfun(@(x) length(x.data.ISIH),stabilityData);
stabilityData = stabilityData(nSpikes >= 125);
disp(size(stabilityData));
%true positives set


pSet = {};
for j = 1: size(stabilityData,1)
    %separate set into fiths
    for i = 1:5
    
        wfStartIndex = 1 + floor(double(size(stabilityData(j).data.waveforms,2))*double(i-1)/5.0);
        wfEndIndex = floor(double(size(stabilityData(j).data.waveforms,2))*double(i)/5.0);
        isiStartIndex =  1 + floor(double(length(stabilityData(j).data.ISIH))*double(i-1)/5.0);
        isiEndIndex = floor(double(length(stabilityData(j).data.ISIH))*double(i)/5.0);
        
        tStruct.data(i).waveforms = stabilityData(j).data.waveforms(:,wfStartIndex:wfEndIndex);
        tStruct.data(i).ISIH = stabilityData(j).data.ISIH(isiStartIndex:isiEndIndex);

    end
    
        tStruct.channel = stabilityData(j).channel;
        tStruct.unit = stabilityData(j).unit;
        
        if isempty(pSet)
            pSet = tStruct;
        else
            pSet = [pSet;tStruct];
        end
end

permIndices = nchoosek(1:5,2);

pValues = zeros(size(permIndices,1)*size(pSet,1),2);
pIndex = 1;
for i = 1: size(pSet,1)
    for j = 1: size(permIndices,1)
        %waveforms
        waveformsA = pSet(i).data(permIndices(j,1)).waveforms;
        waveformsB = pSet(i).data(permIndices(j,2)).waveforms;
        
        %compute original statistic 
        mA = mean(waveformsA,2);
        mB = mean(waveformsB,2);
        To = norm(mA-mB);
        nA = size(waveformsA,2);
        nB = size(waveformsB,2);
        totalSet = [waveformsA, waveformsB];
        %permute sets
        nIter = 1000;
        permT = zeros(nIter,1);
        for t = 1:nIter
            randIndices = randperm(nA+nB);
            setA = totalSet(:,randIndices(1:nA));
            setB = totalSet(:,randIndices(nA+1:nA + nB));
            tmA = mean(setA,2);
            tmB = mean(setB,2);
            permT(t) = norm(tmA-tmB);
            
        end
        pValues(pIndex) = double(length(permT(permT > To) )+1)/double((length(permT)+1));
        pIndex = pIndex + 1;
    end
end


