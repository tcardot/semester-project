function TransformEventForBlackrocks_NSP1NSP2()

mkdir(output_dir);

viconData = get_mat_files(input_vicon_data);
eventsData = get_mat_files(input_Events_data); 
nsp1Data = get_mat_files(input_Brain_data);

%% test for one file
load([input_Events_data eventsData{1}])
load([input_vicon_data viconData{1}])
load([input_Brain_data nsp1Data{1}])

%% Identify Beginning of each movement
ind_NSP1 = find(strcmp(analog.labels,'Voltage.BlackRock NSP1')==1);
iniNSP1_1000 = find(diff(analog.data(:,ind_NSP1))>2)+1;
iniNSP1_100 = round(iniNSP1_1000/analog.framerate*kinematic.framerate);

startMovementVicon = [structTrials.beginningOfMovement];
startMovementNSP1 = startMovementVicon-iniNSP1_100;

endMovementVicon = [structTrials.LightOFF];
endMovementNSP1 = endMovementVicon - iniNSP1_100;
neuronsFiring = firingRate(1).FR;

for ii = 1: size(startMovementNSP1,2)
    spikesProEvent(ii).startEventNSP1 = startMovementNSP1(ii);
    spikesProEvent(ii).endEventNSP1 = endMovementNSP1(ii);
    spikesProEvent(ii).spikesInEvent = neuronsFiring(:,startMovementNSP1(ii):endMovementNSP1(ii));
end