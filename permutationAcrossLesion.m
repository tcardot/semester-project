featsBefore = load('wfIsih20190516');
featsAfter = load('wfIsih20190604');

featsBefore = featsBefore.wfIsih20190516;
featsAfter = featsAfter.wfIsih20190604;

pValues = zeros(size(featsBefore,1),1);
for i = 1:size(featsBefore,1)
 %waveforms
    waveformsA = featsBefore(i).data.waveforms;
    waveformsB = featsAfter(i).data.waveforms;

    %compute original statistic 
    mA = mean(waveformsA,2);
    mB = mean(waveformsB,2);
    To = norm(mA-mB);
    nA = size(waveformsA,2);
    nB = size(waveformsB,2);
    totalSet = [waveformsA, waveformsB];
    %permute sets
    nIter = 1000;
    permT = zeros(nIter,1);
    for t = 1:nIter      
        randIndices = randperm(nA+nB);
        setA = totalSet(:,randIndices(1:nA));
        setB = totalSet(:,randIndices(nA+1:nA + nB));
        tmA = mean(setA,2);
        tmB = mean(setB,2);
        permT(t) = norm(tmA-tmB);

    end
    pValues(i) = double(length(permT(permT > To))+1)/double((length(permT)+1));

end

hist(pValues)