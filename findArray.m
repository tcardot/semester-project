function array = findArray( channel)
%find array: return the the index of the array of the given channel
%INPUT ARGUMENT: 
%channel : index of the channel (1,...,256)
%
%RETURN :
% array: index of the electrode array (1,...,6)

array = 0;
if channel >= 1 && channel <= 48
    array = 1;
elseif channel >= 49 && channel <= 96
    array = 2;
elseif channel >= 97 && channel <= 144
    array = 3;
elseif channel >= 145 && channel <= 176
    array = 4;
elseif channel >= 177 && channel <= 224
    array = 5;
elseif channel >= 225 && channel <= 256
    array = 6;
else
    disp('invalid channel');
end

end

