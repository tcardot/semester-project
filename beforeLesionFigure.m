records = 1:11;
%records(7) = [];
%{
elec1 = extractElectrode(1,'20190604',records);
elec2 = extractElectrode(2,'20190604',records);
elec3 = extractElectrode(3,'20190604',records);
elec4 = extractElectrode(4,'20190604',records);
elec5 = extractElectrode(5,'20190604',records);
elec6 = extractElectrode(6,'20190604',records);
%}

arraysBefore = load('arrays20190516');
arraysBefore = arraysBefore.arrays20190516;

arraysAfter = load('arrays20190604');
arraysAfter = arraysAfter.arrays20190604;


RatesB1 = computeElectrodeFiringRate(arraysBefore(1).array);
RatesB2 = computeElectrodeFiringRate(arraysBefore(2).array);
RatesB3 = computeElectrodeFiringRate(arraysBefore(3).array);
RatesB4 = computeElectrodeFiringRate(arraysBefore(4).array);
RatesB5 = computeElectrodeFiringRate(arraysBefore(5).array);
RatesB6 = computeElectrodeFiringRate(arraysBefore(6).array);

RatesA1 = computeElectrodeFiringRate(arraysAfter(1).array);
RatesA2 = computeElectrodeFiringRate(arraysAfter(2).array);
RatesA3 = computeElectrodeFiringRate(arraysAfter(3).array);
RatesA4 = computeElectrodeFiringRate(arraysAfter(4).array);
RatesA5 = computeElectrodeFiringRate(arraysAfter(5).array);
RatesA6 = computeElectrodeFiringRate(arraysAfter(6).array);


figure();
subplot(2,3,1);
histogram(RatesB1,50);
hold on
histogram(RatesA1,50);
title('M1 LEFT');


subplot(2,3,2);
histogram(RatesB2,20);
hold on
histogram(RatesA2,20);
title('S1 RIGHT');

subplot(2,3,3);
histogram(RatesB3,20);
hold on
histogram(RatesA3,20);
title('PMd RIGHT');

subplot(2,3,4);
histogram(RatesB4,20);
hold on
histogram(RatesA4,20);
title('PMv LEFT');

subplot(2,3,5);
histogram(RatesB5,20);
hold on
histogram(RatesA5,20);
title('M1 RIGHT');

subplot(2,3,6);
histogram(RatesB6,20);
hold on
histogram(RatesA6,20);
title('PMv RIGHT');
hold off
legend('before lesion','after lesion');

%{
subplot(2,3,1);
hist(Rates1,20);
title('array 1');

subplot(2,3,2);
hist(Rates2,20);
title('array 2');

subplot(2,3,3);
hist(Rates3,20);
title('array 3');

subplot(2,3,4);
hist(Rates4,20);
title('array 4');

subplot(2,3,5);
hist(Rates5,20);
title('array 5');

subplot(2,3,6);
hist(Rates6,20);
title('array 6');

%}